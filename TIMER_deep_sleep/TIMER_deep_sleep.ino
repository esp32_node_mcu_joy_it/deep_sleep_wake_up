#include <Arduino.h> 

#define uS_TO_S_FACTOR 1000000  /* conversion microsecondes en secondes */
#define TIME_TO_SLEEP  5        /* temps en seconde pour la période de réveil */
int led =13;
void setup(){
  Serial.begin(115200);
  pinMode(led, OUTPUT);

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);      // réveil de l'ESP32 à l'aide du Timer
  Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) + " Seconds"); // impression sur le terminal série 
  digitalWrite(led, HIGH); // mise à l'état haut de la broche 13
  Serial.println("Going to sleep now");  // impression sur le terminal série pour la mise en veille
  delay(1000);
  Serial.flush(); 
  esp_deep_sleep_start();    // mise en veille deep_sleep
}

void loop(){ 

}
