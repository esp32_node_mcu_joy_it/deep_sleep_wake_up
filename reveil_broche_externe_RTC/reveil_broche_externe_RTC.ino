#include <Arduino.h>

/*PROTOTYPES DE FONCTION*/

void print_wakeup_reason();

int led=13;

void setup(){
  Serial.begin(115200);

  pinMode(led, OUTPUT);
 
  //Reveille le processeur depuis un signal logique externe , ici GPIO34
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_34,1); 
   print_wakeup_reason(); // CONNAITRE LA CAUSE DU REVEIL
    Serial.println("Réveil de l'ESP32");
  digitalWrite(led,HIGH); 
 
  
  while(digitalRead(34)==1)
  {
    
  };

  //Go to sleep now
  Serial.println("mis en veille profonde du module");
  esp_deep_sleep_start();

 
}

void loop(){}

//Function that prints the reason by which ESP32 has been awaken from sleep
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch(wakeup_reason)
  {
    case 1  : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case 2  : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case 3  : Serial.println("Wakeup caused by touchpad"); break;
    case 4  : Serial.println("Wakeup caused by timer"); break;
    case 5  : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.println("Wakeup was not caused by deep sleep"); break;
  }
}
