#include <Arduino.h> 

#define uS_TO_S_FACTOR 1000000  /* conversion microsecondes en secondes */
#define TIME_TO_SLEEP  5        /* temps en seconde pour la période de réveil */
int led =13;


void setup(){
  Serial.begin(115200);
  pinMode(led, OUTPUT);

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);      // réveil de l'ESP32 à l'aide du Timer
  Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) + " Seconds"); // impression sur le terminal série 
  digitalWrite(led, HIGH); // mise à l'état haut de la broche 13
  print_wakeup_reason();  // permet de connaitre l'origine du réveil de l'ESP32
  Serial.println("Going to sleep now");  // impression sur le terminal série pour la mise en veille
  delay(1000);
  Serial.flush(); 
  esp_deep_sleep_start();    // mise en veille deep_sleep
}

/* connaitre l'origine du réveil de l'ESP32*/
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    case ESP_SLEEP_WAKEUP_GPIO : Serial.println("Wakeup caused by GPIO"); break;
    case ESP_SLEEP_WAKEUP_UART : Serial.println("Wakeup caused by UART"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}
void loop(){ 

}
